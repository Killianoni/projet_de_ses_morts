# *HOST OS*

### Nom

``` system_profiler SPHardwareDataType ```
```
Hardware:

    Hardware Overview:

      Model Name: MacBook Pro
      Model Identifier: MacBookPro16,3
      Processor Name: Quad-Core Intel Core i5
      Processor Speed: 1,4 GHz
      Number of Processors: 1
      Total Number of Cores: 4
      L2 Cache (per Core): 256 KB
      L3 Cache: 6 MB
      Hyper-Threading Technology: Enabled
      Memory: 8 GB
      Boot ROM Version: 1037.147.4.0.0 (iBridge: 17.16.16610.0.0,0)
      Serial Number (system): C02D22R5P3Y1
      Hardware UUID: 625C7ED8-FDD6-5AB3-8327-85EB62EFE2E2
      Activation Lock Status: Enabled
```

### OS

```system_profiler SPSoftwareDataType```

```
Software:

    System Software Overview:

      System Version: macOS 10.15.7 (19H2)
      Kernel Version: Darwin 19.6.0
      Boot Volume: Macintosh HD
      Boot Mode: Normal
      Computer Name: MacBook Pro de Killian
      User Name: Killian Adonaï (killianoni)
      Secure Virtual Memory: Enabled
      System Integrity Protection: Enabled
      Time since boot: 20:41
```

### Architecture

```uname -m```

```x86_64```

### RAM

```system_profiler SPMemoryDataType```

```
Memory:

    Memory Slots:

      ECC: Disabled
      Upgradeable Memory: No

        BANK 0/ChannelA-DIMM0:

          Size: 4 GB
          Type: LPDDR3
          Speed: 2133 MHz
          Status: OK
          Manufacturer: Samsung
          Part Number: K4E6E304ED-EGCG
          Serial Number: 55000000

        BANK 2/ChannelB-DIMM0:

          Size: 4 GB
          Type: LPDDR3
          Speed: 2133 MHz
          Status: OK
          Manufacturer: Samsung
          Part Number: K4E6E304ED-EGCG
          Serial Number: 55000000
```
Je n'ai pu trouver que ca

# *DEVICES*

### Processeur

```sysctl -n machdep.cpu.brand_string```

```Intel(R) Core(TM) i5-8257U CPU @ 1.40GHz```



```system_profiler SPHardwareDataType```

```
Hardware:

    Hardware Overview:

      Model Name: MacBook Pro
      Model Identifier: MacBookPro16,3
      Processor Name: Quad-Core Intel Core i5
      Processor Speed: 1,4 GHz
      Number of Processors: 1
      Total Number of Cores: 4
      L2 Cache (per Core): 256 KB
      L3 Cache: 6 MB
      Hyper-Threading Technology: Enabled
      Memory: 8 GB
      Boot ROM Version: 1037.147.4.0.0 (iBridge: 17.16.16610.0.0,0)
      Serial Number (system): C02D22R5P3Y1
      Hardware UUID: 625C7ED8-FDD6-5AB3-8327-85EB62EFE2E2
      Activation Lock Status: Enabled
```

* Intel(R) Core(TM) : Correspond simplement à la marque
* i5 : Correpsond à la version du processeur
* 8 : Désigne la génération du processeur
* 257 : Est le SKU number il correspond à l'ordre de production des processeur de la même génération
* U : Est le suffixe il dans ce cas "U" désigne un processeur fabriqué pour les ordinateurs portables
* 1.40 GHz : Fréquence du processeur


### Trackpad

Recherches effectuées mais on ne m'indique seulement comment turn on/off le trackpad :/


### GPU

```system_profiler SPDisplaysDataType```

```
Graphics/Displays:

    Intel Iris Plus Graphics 645:

      Chipset Model: Intel Iris Plus Graphics 645
      Type: GPU
      Bus: Built-In
      VRAM (Dynamic, Max): 1536 MB
      Vendor: Intel
      Device ID: 0x3ea6
      Revision ID: 0x0001
      Metal: Supported, feature set macOS GPUFamily2 v1
      Displays:
        Color LCD:
          Display Type: Built-In Retina LCD
          Resolution: 2560 x 1600 Retina
          Framebuffer Depth: 24-Bit Color (ARGB8888)
          Main Display: Yes
          Mirror: Off
          Online: Yes
          Automatically Adjust Brightness: No
          Connection Type: Internal
```
### *DD*


### Modèle

```diskutil info /dev/disk0 | grep 'Device / Media Name'```
```Device / Media Name:       APPLE SSD AP0256N```

```diskutil info /dev/disk1 | grep 'Device / Media Name'```
```Device / Media Name:       APPLE SSD AP0256N```


### Partitions/System File

```diskutil list```


```
/dev/disk0 (internal, physical):
   #:                       TYPE NAME                    SIZE       IDENTIFIER
   0:      GUID_partition_scheme                        *251.0 GB   disk0
   1:                        EFI EFI                     314.6 MB   disk0s1
   2:                 Apple_APFS Container disk1         250.7 GB   disk0s2

/dev/disk1 (synthesized):
   #:                       TYPE NAME                    SIZE       IDENTIFIER
   0:      APFS Container Scheme -                      +250.7 GB   disk1
                                 Physical Store disk0s2
   1:                APFS Volume Macintosh HD            11.2 GB    disk1s1
   2:                APFS Volume Macintosh HD - Data     64.4 GB    disk1s2
   3:                APFS Volume Preboot                 82.0 MB    disk1s3
   4:                APFS Volume Recovery                529.0 MB   disk1s4
   5:                APFS Volume VM                      3.2 GB     disk1s5
```



# *USERS*

### Liste
```dscl . list /Users```

```
_amavisd
_analyticsd
_appleevents
_applepay
_appowner
_appserver
_appstore
_ard
_assetcache
_astris
_atsserver
_avbdeviced
_calendar
_captiveagent
_ces
_clamav
_cmiodalassistants
_coreaudiod
_coremediaiod
_ctkd
_cvmsroot
_cvs
_cyrus
_datadetectors
_devdocs
_devicemgr
_displaypolicyd
_distnote
_dovecot
_dovenull
_dpaudio
_driverkit
_eppc
_findmydevice
_fpsd
_ftp
_gamecontrollerd
_geod
_hidd
_iconservices
_installassistant
_installer
_jabber
_kadmin_admin
_kadmin_changepw
_krb_anonymous
_krb_changepw
_krb_kadmin
_krb_kerberos
_krb_krbtgt
_krbfast
_krbtgt
_launchservicesd
_lda
_locationd
_lp
_mailman
_mbsetupuser
_mcxalr
_mdnsresponder
_mobileasset
_mysql
_nearbyd
_netbios
_netstatistics
_networkd
_nsurlsessiond
_nsurlstoraged
_ondemand
_postfix
_postgres
_qtss
_reportmemoryexception
_sandbox
_screensaver
_scsd
_securityagent
_softwareupdate
_spotlight
_sshd
_svn
_taskgated
_teamsserver
_timed
_timezone
_tokend
_trustevaluationagent
_unknown
_update_sharing
_usbmuxd
_uucp
_warmd
_webauthserver
_windowserver
_www
_wwwproxy
_xserverdocs
daemon
killianoni
nobody
root
```


### Admin

```dscl . -read /Groups/admin GroupMembership```

```GroupMembership: root killianoni```


# *PROCESSUS*

### Services Système 

(n'affiche que les noms)
```ps axc | sed "s/.*:..... /\"/" | sed s/$/\"/``` 
```
 PID   TT  STAT      TIME COMMAND"
"launchd"
"syslogd"
"UserEventAgent"
"uninstalld"
"kextd"
"fseventsd"
"mediaremoted"
"systemstats"
"configd"
"endpointsecurityd"
"powerd"
"logd"
"keybagd"
"watchdogd"
"mds"
"iconservicesd"
"diskarbitrationd"
"coreduetd"
"opendirectoryd"
"apsd"
"launchservicesd"
"timed"
"usbmuxd"
"securityd"
"auditd"
"autofsd"
"displaypolicyd"
"dasd"
"PerfPowerServices"
"logind"
"revisiond"
"KernelEventAgent"
"bluetoothd"
"hidd"
"sandboxd"
"corebrightnessd"
"AirPlayXPCHelper"
"notifyd"
"distnoted"
"syspolicyd"
"cfprefsd"
"coreservicesd"
"tccd"
"aslmanager"
"loginwindow"
"authd"
"contextstored"
"trustd"
"nehelper"
"coreaudiod"
"analyticsd"
"ocspd"
"mobileassetd"
"com.apple.audio.DriverHelper"
"WindowServer"
"symptomsd"
"mDNSResponder"
"mDNSResponderHelper"
"awdd"
"airportd"
"com.apple.geod"
"secinitd"
"cfprefsd"
"nsurlsessiond"
"trustd"
"searchpartyd"
"lsd"
"backupd-helper"
"diskmanagementd"
"AudioComponentRegistrar"
"com.apple.audio.SandboxHelper"
"smd"
"AppleUserECM"
"AppleUserHIDDrivers"
"AppleUserHIDDrivers"
"AppleUserHIDDrivers"
"AppleUserHIDDrivers"
"AppleUserHIDDrivers"
"AppleUserHIDDrivers"
"AppleUserHIDDrivers"
"AppleUserHIDDrivers"
"AppleUserHIDDrivers"
"AppleUserHIDDrivers"
"AppleUserHIDDrivers"
"distnoted"
"AppleUserHIDDrivers"
"AppleUserHIDDrivers"
"AppleUserHIDDrivers"
"AppleUserECMData"
"runningboardd"
"com.apple.CodeSigningHelper"
"mds_stores"
"locationd"
"secinitd"
"TouchBarServer"
"CVMServer"
"AppleUserHIDDrivers"
"colorsync.displayservices"
"colorsyncd"
"multiversed"
"netbiosd"
"com.apple.ifdreader"
"apfsd"
"appleeventsd"
"usbd"
"thermald"
"VDCAssistant"
"sysmond"
"bosUpdateProxy"
"SubmitDiagInfo"
"ViewBridgeAuxiliary"
"bootinstalld"
"systemstats"
"distnoted"
"com.apple.cmio.registerassistantservice"
"com.apple.AccountPolicyHelper"
"GSSCred"
"coreauthd"
"coreauthd"
"cfprefsd"
"securityd_service"
"biometrickitd"
"biokitaggdd"
"UserEventAgent"
"com.apple.sbd"
"distnoted"
"knowledge-agent"
"universalaccessd"
"lsd"
"secd"
"trustd"
"talagent"
"Dock"
"SystemUIServer"
"Finder"
"cloudd"
"backgroundtaskmanagementagent"
"QuickLookUIService"
"pkd"
"secinitd"
"systemsoundserverd"
"pboard"
"dmd"
"tccd"
"nsurlsessiond"
"ViewBridgeAuxiliary"
"CategoriesService"
"ContextService"
"sharedfilelistd"
"bird"
"AMPDeviceDiscoveryAgent"
"accountsd"
"fontd"
"CloudKeychainProxy"
"rapportd"
"nsurlstoraged"
"routined"
"usernoted"
"ControlStrip"
"networkserviceproxy"
"lockoutagent"
"identityservicesd"
"APFSUserAgent"
"CalendarAgent"
"assistantd"
"swcd"
"ScreenTimeAgent"
"CommCenter"
"CategoriesService"
"homed"
"ContextStoreAgent"
"NotificationCenter"
"filecoordinationd"
"TrustedPeersHelper"
"CalNCService"
"mapspushd"
"WiFiAgent"
"neagent"
"ctkd"
"WirelessRadioManagerd"
"callservicesd"
"ctkd"
"imagent"
"ContainerMetadataExtractor"
"AppSSOAgent"
"IMDPersistenceAgent"
"corespeechd"
"SocialPushAgent"
"imklaunchagent"
"Siri"
"icdd"
"P72E3GC48.com.dashlane.DashlaneAgent"
"watchman"
"askpermissiond"
"sharingd"
"AirPlayUIAgent"
"cloudpaird"
"diagnostics_agent"
"TextInputMenuAgent"
"remindd"
"amsaccountsd"
"ctkahp"
"amfid"
"taskgated"
"ctkahp"
"akd"
"ProtectedCloudKeySyncing"
"iconservicesagent"
"Spotlight"
"useractivityd"
"storeaccountd"
"com.apple.dock.extra"
"CategoriesService"
"IMRemoteURLConnectionAgent"
"IMRemoteURLConnectionAgent"
"AssetCacheLocatorService"
"IMRemoteURLConnectionAgent"
"parsecd"
"ContactsAccountsService"
"AssetCache"
"adid"
"CallHistoryPluginHelper"
"WiFiVelocityAgent"
"wifivelocityd"
"parsec-fbf"
"commerce"
"com.apple.geod"
"CategoriesService"
"mediaremoteagent"
"familycircled"
"findmydeviced"
"CoreLocationAgent"
"pbs"
"fmfd"
"com.apple.iCloudHelper"
"ReportCrash"
"DataDetectorsSourceAccess"
"AssetCacheTetheratorService"
"deleted"
"cdpd"
"com.apple.siri.ClientFlow.ClientScripter"
"PAH_Extension"
"EmojiFunctionRowIM_Extension"
"AMPLibraryAgent"
"com.apple.CharacterPicker.FileService"
"nsurlstoraged"
"CMFSyncAgent"
"adprivacyd"
"SafeEjectGPUAgent"
"SafeEjectGPUService"
"AudioComponentRegistrar"
"IMRemoteURLConnectionAgent"
"SidecarRelay"
"corespotlightd"
"findmydevice-user-agent"
"CrashReporterSupportHelper"
"com.apple.audio.SandboxHelper"
"com.apple.siri.embeddedspeech"
"nfcd"
"followupd"
"fpsd"
"exchangesyncd"
"suggestd"
"akd"
"DashlanePluginMASService"
"storagekitd"
"assistant_service"
"com.apple.hiservices-xpcservice"
"deleted_helper"
"com.apple.AmbientDisplayAgent"
"replayd"
"system_installd"
"coresymbolicationd"
"com.apple.quicklook.ThumbnailsAgent"
"storedownloadd"
"fileproviderd"
"installd"
"PodcastContentService"
"CoreServicesUIAgent"
"com.apple.BKAgentService"
"PowerChime"
"ScopedBookmarkAgent"
"iconservicesagent"
"SoftwareUpdateNotificationManager"
"softwareupdated"
"suhelperd"
"siriknowledged"
"Safari"
"com.apple.Safari.History"
"com.apple.WebKit.Networking"
"passd"
"SafariExtensionMAS"
"SafariBookmarksSyncAgent"
"ImageIOXPCService"
"SafariLaunchAgent"
"SafariCloudHistoryPushAgent"
"com.apple.WebKit.WebContent"
"seld"
"com.apple.Safari.SearchHelper"
"com.apple.Safari.SafeBrowsing.Service"
"rtcreportingd"
"com.apple.WebKit.WebContent"
"com.apple.WebKit.WebContent"
"trustd"
"AppleSpell"
"keyboardservicesd"
"recentsd"
"com.apple.WebKit.WebContent"
"com.apple.WebKit.Networking"
"Dashlane"
"dprivacyd"
"SiriNCService"
"com.apple.speech.speechsynthesisd"
"com.apple.audio.SandboxHelper"
"QuickLookSatellite"
"com.apple.WebKit.WebContent"
"CategoriesService"
"com.apple.WebKit.WebContent"
"com.apple.WebKit.WebContent"
"mobileactivationd"
"devicecheckd"
"FMIPClientXPCService"
"com.apple.PerformanceAnalysis.animationperfd"
"com.apple.WebKit.WebContent"
"tzd"
"CategoriesService"
"Terminal"
"com.apple.DictionaryServiceHelper"
"kextcache"
"com.apple.WebKit.WebContent"
"com.apple.accessibility.mediaaccessibilityd"
"com.apple.WebKit.WebContent"
"com.apple.WebKit.WebContent"
"com.apple.audio.SandboxHelper"
"lskdd"
"EscrowSecurityAlert"
"AssetCacheLocatorService"
"com.apple.WebKit.WebContent"
"MTLCompilerService"
"com.apple.WebKit.WebContent"
"com.apple.WebKit.WebContent"
"VTDecoderXPCService"
"com.apple.audio.SandboxHelper"
"CAReportingService"
"com.apple.WebKit.WebContent"
"OSDUIHelper"
"com.apple.WebKit.WebContent"
"syncdefaultsd"
"com.apple.CloudDocs.MobileDocumentsFileProvider"
"mdworker_shared"
"login"
"-zsh"
"ps"
"sed"
"sed"
```

* "akd" est le AuthKit il permet de se log à la machine, icloud et d'autres services.
* "thermald" est un gestionnaire de température il permet de garder la machine dans des températures adéquates
* "powerd" permet de gérer la mise en veille de l'écran sur batterie ou secteur
* "installd" permet de mettre à jours et gérer l'installation des programmes
* "uninstalld" permet quant à lui de désinstaller les programmes
(le d signifie daemon ce sont les applications qui fonctionnent en arrière plan)

# *NETWORK*

### CARTES RESEAUX

```networksetup -listallhardwareports```

```
Hardware Port: USB 10/100/1000 LAN
Device: en5
Ethernet Address: 00:e0:4c:68:0b:2d

Hardware Port: Wi-Fi
Device: en0
Ethernet Address: 14:7d:da:10:65:78

Hardware Port: Bluetooth PAN
Device: en4
Ethernet Address: 3c:22:fb:e1:a5:cb

Hardware Port: Thunderbolt 1
Device: en1
Ethernet Address: 82:bd:d0:25:10:01

Hardware Port: Thunderbolt 2
Device: en2
Ethernet Address: 
```
* LAN : port éthernet
* WIFI : carte WIFI
* Bluetooth : connexion bluetooth
* thunderbolt : les 2 ports du mac


### TCP UDP


# SCRIPTING
```
clear
printf "\n"
echo --------------- OS INFORMATIONS ---------------
printf "\n"

#Nom de la machine
machine=$(system_profiler SPHardwareDataType | sed -n '5p' | cut -c 19-29)

echo Nom de la machine : $machine
printf "\n"

#Personnal IP
ip=$(ipconfig getifaddr en0)

echo Mon ip est : $ip
printf "\n"

#OS Version
osvers=$(system_profiler SPSoftwareDataType  | sed -n '5p' | cut -c 23-45)

echo La version de mon OS est : $osvers
printf "\n" 

upsince=$(uptime | cut -c 10-15)

echo Mon pc est up depuis : $upsince
printf "\n"

#OS à jour
echo Checking if OS is up to date ...
printf "\n"
osup=$(softwareupdate -l | grep  "*\|No")
if [ "$osup" == "No" ]; then
osres=$(echo Oui)
    else
        osres=$(echo Non)
fi
echo Est ce que mon OS est à jour ? : $osres
printf "\n"

#RAM used/ RAM free
usedram=$(top -l 1 | grep -E "^Phys" | cut -c 10-15)
unusedram=$(top -l 1 | grep -E "^Phys" | cut -c 22-27)

echo RAM :
printf "\t"
echo Used : $usedram
printf "\t"
echo Unused : $unusedram

#Disk used/ Disk free
disk1=$(df -k -h /System | sed -n '2p' | cut -c 21-26)
disk2=$(df -k -h /Library | sed -n '2p' | cut -c 21-26)
diskfree=$(df -k -h /Library | sed -n '2p' | cut -c 29-33)

echo DISK :
printf "\t"
echo Used : $disk2 + $disk1
printf "\t"
echo Unused : $diskfree
printf "\n"

#User list
echo Utilisateurs :
printf "\n"
printf "\t"
echo $(dscl . list /Users) | sed 's/ /; /g'
printf "\n"

#Ping delay 8.8.8.8
echo Getting average ping latency to 8.8.8.8 server ...
delay=$(ping -c 5 8.8.8.8 | sed -n '10p' | cut -c 40-45)
printf "\n"
echo Le temps moyen pour acceder au serveur est de : $delay ms
printf "\n"

echo --------------- END ---------------
printf "\n"
```

```
#Countdown
sec=$2
clear
echo Countdown :
    while [ $sec -ge 0 ]; do
        echo -ne "$sec\033[0K\r"
            let "sec=sec-1"
                sleep 1
                    done
clear

#Execution
if [ "$1" == "shutdown" ]; then

#Shutdown
       sudo shutdown -h now
        
        
#Lock
elif [ "$1" == "lock" ]; then
       /System/Library/CoreServices/Menu\ Extras/User.menu/Contents/Resources/CGSession -suspend
       
#Error
       else
       echo Error wrong arguments, should be shutdown or lock with a delay
fi
```

# SOFTS

# PAQUETS

Le gestionnaire permet d'installer, désinstaller, mettre à jour des logiciels.

C'est une facon beaucoup plus sécurisé d'installer car on ne va pas chercher le logiciel sur internet et on a donc moins de chance d'avoir un fichier corrompu. Cela permet d'éviter aux hackers d'obtenir des données du client ou de l'éditeur.


# LIST

```brew list```
``` 
gdbm        icu4c        openjdk        python@3.9    xz
gettext        jq        openssl@1.1    readline
git        node        pcre        sqlite
go        oniguruma    pcre2        watchman
```

```brew info wget```
```
wget: stable 1.20.3 (bottled), HEAD
Internet file retriever
https://www.gnu.org/software/wget/
Not installed
From: https://github.com/Homebrew/homebrew-core/blob/HEAD/Formula/wget.rb
License: GPL-3.0-or-later
==> Dependencies
Build: pkg-config ✘
Required: libidn2 ✘, openssl@1.1 ✔
==> Options
--HEAD
    Install HEAD version
==> Analytics
install: 93,106 (30 days), 287,630 (90 days), 1,284,189 (365 days)
install-on-request: 91,918 (30 days), 283,895 (90 days), 1,240,223 (365 days)
build-error: 0 (30 days)
```

l'installation se fait par lien github

